# Calving glacier hazard in Greenland

**SC leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/CNRS.png?inline=false" width="80">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/CSC.png?inline=false" width="200">

**Codes:** ELMER/ICE

**Target EuroHPC Architectures:** LUMI-C

**Type:** Hazard assessment

## Description

We will simulate the glacier dynamics on a short timescale at Bowdoin glacier
situated nearby the village of Qaanaaq using Elmer/Ice. We will simulate iceberg
calving using simple models and finite element methods coupling iceberg
capsizing simulation with a coupling of seismic analysis and numerical modelling
of the physical processes. The calving of icebergs and its interaction with the
surrounding water will be performed outside of the ChEESE project and will be
used as a boundary condition at the glacier terminus simulated using Elmer-Ice.

<img src="SC9.2.png" width="800">

**Figure 3.15.1.** Graphical abstract of SC9.2 showing the initial ice-flow simulation of the front-area of Bowdoin glacier with Elmer/Ice (setup courtesy Eef van Dongen,
ETHZ/SMHI) – vertical scales are exaggerated by a factor 2.

## Expected results

Quantification of the hazard related to iceberg calving near the Bowdoin glacier in terms of generated
water waves, sea ice cover and ocean layers perturbation. Seismic based detection of large iceberg calving that will be
applicable near other marine terminating glaciers. Constrain on glacier stability and basal friction laws based on innovative
coupling of seismic analysis and numerical modeling of the physical processes.